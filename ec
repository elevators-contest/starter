echo Checking for updates... \
 && docker pull --quiet registry.gitlab.com/elevators-contest/eccli:latest \
 && echo Starting eccli... \
 && docker run --rm \
    -v "$PWD"/build/classes/java/main:/app/resources \
    -v "$PWD"/.ec:/.ec \
    registry.gitlab.com/elevators-contest/eccli:latest \
    "$@"
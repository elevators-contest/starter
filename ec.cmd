echo off
echo Checking for updates... ^
 && docker pull --quiet registry.gitlab.com/elevators-contest/eccli:latest ^
 && echo Starting eccli... ^
 && docker run --rm ^
    -v "%cd%"/build/classes/java/main:/app/resources ^
    -v "%cd%"/.ec:/.ec ^
    registry.gitlab.com/elevators-contest/eccli:latest ^
    %*
#### Problem to solve

You shall develop a piece of software that controls movements of elevators in a building.
Your software should react to events occurring in the building:
* people calling elevators to particular floors
* people pressing buttons inside elevators to go to particular floors 

In reaction to such events, your software should schedule elevators of the building
to travel to where they are needed, so that people are served as efficiently as possible. 

#### Developing your solution

A solution is simply an implementation of `elevators.domain.Router` interface.
This project contains a silly starting point: `contest.solution.MyLousyRouter`

Please inspect `MyLousyRouter.bindTo(Building)` method to learn how to watch the events and schedule elevator movements.
Your job is to provide an implementation of `bindTo()` method that will allow serving the building efficiently. 

You can open a simulation of a 'sample' building that is using your router. Run `MyLousyRouter.main()` to see it. 

#### Testing your solution

In order to check how your implementation behaves, you can use the GUI simulator again and again, 
manually clicking through the situations that you want your solution to properly address.

Care to automate such tests? Have a look at `MyLousyRouterTest`.

Most important part of automated testing is expressing the scenario that you want your router to solve.
You can do it by providing an implementation of `elevators.domain.TrafficGenerator`. 
See an example in `MyLousyRouterTest.allElevatorsUp()`, it uses a built-in TrafficGenerator with a fluent interface, 
allowing you to specify what events should occur at what points in time of the simulation. 

#### Participating in a contest

You can submit your solution to a contest, where it will be tested against predefined scenarios and compared with solutions of other contestants.

_Prerequisite: you need Docker installed to use `ec`. get it [here](https://docs.docker.com/get-docker)_ 

This project comes with ElevatorsContest CLI tool.
Run it in project directory: `ec --help`

You will first need to register to a contest with `ec register`. Your contest administrator should provide you a contest-id to register to.

Then you can submit your solution with `ec submit`

Scoreboard is available via `ec scoreboard`

#### Troubleshooting

##### Docker needs sudo

On Linux `docker` is usually not sharing the same group as your user.
If that's your case, you may need to prefix all invocations of `ec` with `sudo`, e.g. `sudo ./ec --help`.

If you want to run docker without sudo, see [here](https://docs.docker.com/engine/install/linux-postinstall/)

##### Compilation fails due to /build directory being readonly

In case you happen to execute any `ec` command before compiling the code, or after `gradlew clean`, 
you will find that /build directory is created with readonly access, preventing saving any files into it, 
and consequently failing compilation attempts. In such case you can simply delete /build with `sudo rm -rf`.

Then just compile your code (`gradlew classes`), and the directory will be properly writable.
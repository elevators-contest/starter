package contest.solution;

import elevators.domain.Building;
import elevators.domain.BuildingConfig;
import elevators.domain.Elevator;
import elevators.domain.Router;
import elevators.runner.gui.GuiRunner;


public class MyLousyRouter implements Router {

    public static void main(String[] args) {
        GuiRunner.runWith(new MyLousyRouter(), new BuildingConfig(10, 5));
    }

    @Override
    public void bindTo(Building building) {
        building.watch()
                .whenElevatorButtonPressed(Elevator::scheduleGoto);
        building.watch()
                .whenFloorButtonPressed(floor -> building.elevator(0).scheduleGoto(floor));
    }

}

package contest.solution;

import elevators.domain.BuildingConfig;
import elevators.domain.TrafficGenerator;
import elevators.runner.ConsoleRunner;
import org.junit.jupiter.api.Test;

import static elevators.domain.Traffic.elevatorButton;
import static elevators.domain.TrafficGenerator.predefined;
import static org.assertj.core.api.Assertions.assertThat;

class MyLousyRouterTest {

    @Test
    void worksForSimplestScenario() {

        var simulationResults = ConsoleRunner.runWith(
                MyLousyRouter.class.getName(),
                new BuildingConfig(10, 5),
                allElevatorsUp(),
                100,
                true // < -------- change this to false if you'd like to see execution logs (but it makes tests slower)
                );

        //example metric: we expect that the above scenario & configuration will be resolved much before the 'tickLimit'.
        //all elevators need to go from floor 0 to 9 and open the door to 'score'
        assertThat(simulationResults.elapsedTicks()).isLessThan(12);

        //you can also assert on other properties of simulationResults
    }

    // This TrafficGenerator simulates pressing button 'Goto floor 9' in elevators 0-4 at the beginning of the simulation (onTick(0)).
    // It assumes that there are at least 10 floors and 5 elevators in the building.
    private TrafficGenerator allElevatorsUp() {
        return predefined()
                .onTick(0).willPress(
                        elevatorButton(0, 9)
                        .andElevatorButton(1, 9)
                        .andElevatorButton(2, 9)
                        .andElevatorButton(3, 9)
                        .andElevatorButton(4, 9)
                );
    }
}
